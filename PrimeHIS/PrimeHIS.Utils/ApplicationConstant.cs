﻿namespace PrimeHIS.Utils
{
    public class ApplicationConstant
    {
        public const string PhonenumberRegexValidator = @"^[0-9]{4,16}$";

        public const string NumberWithDecimalRegexValidator =
            @"^(\d{1,3}'(\d{3}')*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{3})?)$";

       
    }
}