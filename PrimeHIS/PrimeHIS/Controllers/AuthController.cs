﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrimeHIS.Dto;
using PrimeHIS.Dto.Auth;
using PrimeHIS.Helper.JWT;
using PrimeHIS.Services.Infrastructure;
using PrimeHIS.Utils;

namespace PrimeHIS.Controllers
{
    public class AuthController : BaseController
    {
        #region Constructor

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        #endregion

        #region Members

        private readonly IAuthService _authService;
        private TResponse _response = new();

        #endregion

        #region ActionMethod

        [HttpPost]
        public TResponse Login(AuthenticateRequest model)
        {
            var (authResponse, keyValue) = _authService.Authenticate(model, IpAddress());
            switch (keyValue)
            {
                case 0:
                    _response.ResponseCode = StatusCodes.Status403Forbidden;
                    _response.ResponseStatus = true;
                    _response.ResponseMessage = ResponseMessage.InvalidUser;
                    break;
                case 1:
                    _response.ResponseCode = StatusCodes.Status200OK;
                    _response.ResponseStatus = true;
                    _response.ResponseMessage = ResponseMessage.Success;
                    _response.ResponsePacket = authResponse;
                    break;
                case 2:
                    _response.ResponseCode = StatusCodes.Status200OK;
                    _response.ResponseStatus = false;
                    _response.ResponseMessage = "User already logged in do you want to continue.";
                    break;
                case 3:
                    _response.ResponseCode = StatusCodes.Status401Unauthorized;
                    _response.ResponseStatus = true;
                    _response.ResponseMessage = ResponseMessage.AuthenticationFail;
                    break;
                case 4:
                    _response.ResponseCode = StatusCodes.Status401Unauthorized;
                    _response.ResponseStatus = false;
                    _response.ResponseMessage = ResponseMessage.MaxUnsuccessLogin;
                    break;
                case -1:
                    _response.ResponseCode = StatusCodes.Status500InternalServerError;
                    _response.ResponseStatus = false;
                    _response.ResponseMessage = ResponseMessage.Error;
                    break;
            }
            return _response;
        }
        
        
      
        [Authorize]
        [HttpGet]
        public TResponse GetUser()
        {
            _response.ResponseCode = (int) HttpStatusCode.OK;
            _response.ResponsePacket = CurrentUser;
            _response.ResponseStatus = true;

            return _response;
        }

    
        #endregion




        #region  Methods
    

        private string IpAddress()
        {
            return Request.Headers.ContainsKey("X-Forwarded-For")
                ? (string) Request.Headers["X-Forwarded-For"]
                : HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
        

        #endregion
    }
}