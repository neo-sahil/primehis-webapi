﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeHIS.Dto
{
    public class TResponse
    {
        public int ResponseCode { get; set; }
        public bool ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
        public object ResponsePacket { get; set; }
    }

    public static class ResponseMessage
    {
        public static string Save = "Record saved successfully";
        public static string MaxUnsuccessLogin = "Maximum unsuccessful login attempts reached. Please try again in 15 minutes.";
        public static string Error = "Something went wrong !!";
        public static string Success = "Successfully.";
        public static string AuthenticationFail = "Authentication fail.";
        public static string InvalidUser = "Invalid User.";
     
    }
}